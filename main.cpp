#include <limits>
#include <string>
#include <vector>
#include <cctype>
#include <cstdlib>
#include <iostream>
#include "Shop.h"
#include "Client.h"
#include "Item.h"

#ifdef _DEBUG
#include "TEST.h"
#endif // _DEBUG


//#define InterFace //Enables user input


using namespace std;

template <class T>
void listItems(const vector<Item<T> >&);
template <class T>
void listClients(const vector<Client<T> >&);
void printcmd();
template <class T>
void cokolwiek();

int main()
{
#ifdef TEST_H_INCLUDED
    testall();
    cout << "Hello world!" << endl;
#endif // _DEBUG
#ifdef InterFace
    cout << "Press any button to start" << endl;
    char c;
    cin >> c;
    cokolwiek<double>();
#endif //InterFace
    return 0;
}




void printcmd()
{
    cout <<\
         "C - create new client" << endl <<\
         "L - list created clients" << endl <<\
         "I - create new item" << endl <<\
         "T - list created items" << endl <<\
         "O - order new item" << endl <<\
         "B - buy item from shop" << endl <<\
         "H - prints these commands" << endl;
}


template <class T>
void cokolwiek()
{
    cout << "Please type the name of the shop: ";
    string shop_name;
    do
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cin >> shop_name;
    }
    while(cin.fail());
    cout << "Please type the name of the seller: ";
    string seller_name;
    do
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cin >> seller_name;
    }
    while(cin.fail());
    cout << "Please type the starting amount of money of the seller: ";
    T seller_balance;
    do
    {
        cin.clear();
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cin >> seller_balance;
    }
    while(cin.fail() || seller_balance<=0);
    Shop<T> shop(shop_name, seller_name, seller_balance);
    vector<Client<T> > cv;
    vector<Item<T> > iv;
    printcmd();
    bool b = true;
    while (b)
    {
        char c;
        do
        {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(),'\n');
            cin >> c;
        }
        while(cin.fail());
        switch (toupper(c))
        {
        case 'C':
        {
            cout << "Input client name: ";
            string cname;
            do
            {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cin >> cname;
            }
            while (cin.fail());
            cout << "Input his balance: ";
            T cbal;
            do
            {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cin >> cbal;
            }
            while (cin.fail() || cbal<=0);
            cv.push_back(Client<T>(cbal, cname));
            break;
        }
        case 'L':
        {
            listClients(cv);
            break;
        }
        case 'I':
        {
            cout << "Input item name: ";
            string iname;
            do
            {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cin >> iname;
            }
            while (cin.fail());
            cout << "Input it's price: ";
            T ibal;
            do
            {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cin >> ibal;
            }
            while (cin.fail() || ibal<=0);
            iv.push_back(Item<T>(ibal, iname));
            break;
        }
        case 'T':
        {
            listItems(iv);
            break;
        }
        case 'O':
        {
            cout << "Do you want to order existing item or new one?" << endl <<\
                 "E - existing" << endl <<\
                 "N - new one" << endl;
            char k;
            do
            {
                cin.clear();
                cin.ignore(numeric_limits<streamsize>::max(),'\n');
                cin >> k;
            }
            while(cin.fail()||!(toupper(k)=='E' || toupper(k)=='N'));
            if (toupper(k)=='E')
            {
                listItems(iv);
                if (!iv.empty())
                {
                    cout << "Which item you want to add? ID: ";
                    unsigned int j;
                    do
                    {
                        cin.clear();
                        cin.ignore(numeric_limits<streamsize>::max(),'\n');
                        cin >> j;
                    }
                    while(cin.fail() || j>= iv.size());
                    shop.orderNewItem(iv[j]);
                }
            }
            else if (toupper(k)=='N')
            {
                cout << "Type item name: ";
                string iname;
                do
                {
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(),'\n');
                    cin >> iname;
                }
                while(cin.fail());
                cout << "Price: ";
                T iprice;
                do
                {
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(),'\n');
                    cin >> iprice;
                }
                while(cin.fail() || iprice <=0);
                shop.orderNewItem(Item<T>(iprice, iname));
            }
            else
            {
                cout << "IT WAS NOT SUPPOSED TO HAPPEN" << endl;
                exit(EXIT_FAILURE);
            }
            break;
        }
        case 'B':
        {
            listClients(cv);
            if (!cv.empty())
            {
                cout << "Type client ID: ";
                unsigned int i;
                do
                {
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(),'\n');
                    cin >> i;
                }
                while (cin.fail() || i>=cv.size());
                shop.listStock();
                cout << "What you want to buy?" << endl;
                string inam;
                do
                {
                    cin.clear();
                    cin.ignore(numeric_limits<streamsize>::max(),'\n');
                    cin >> inam;
                }
                while (cin.fail());
                cv[i].buy(shop, inam);
            }
            break;
        }
        case 'H':
        {
            printcmd();
            break;
        }
        default:
        {
            b = false;
        }
        }
    }


}



template <class T>
void listClients(const vector<Client<T> >& cv)
{
    if (!cv.empty())
    {
        for(unsigned int i = 0; i<cv.size(); i++)
        {
            cout << i << "  " << cv[i].get_name() << "has " << cv[i].get_balance() << " money." << endl;
        }
    }
    else
    {
        cout << "No clients created" << endl;
    }
}

template <class T>
void listItems(const vector<Item<T> >& iv)
{
    if (!iv.empty())
    {
        for(unsigned int i = 0; i<iv.size(); i++)
        {
            cout << i << "  " << iv[i] << endl;
        }
    }
    else
    {
        cout << "No items created" << endl;
    }
}



