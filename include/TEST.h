#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED

void testall();
void testShopOperInt();
void typicalScenarioInt();
void staticTestOuterInt();
template <class T>
void staticTestInner(Shop<T>&,Item<T>&);

template <class T>
void testShopOperDouble(T);
template <class T>
void typicalScenarioDouble(T);


#endif // TEST_H_INCLUDED
