#ifndef CLIENT_H
#define CLIENT_H
#include <vector>
#include "Item.h"
#include "Shop.h"
#include <string>
template <class T>
class Client
{
public:
    Client(T, std::string);
    virtual ~Client();
    T get_balance() const
    {
        return balance;
    }
    std::vector<Item<T>*> get_items() const
    {
        return items;
    }
    std::string get_name() const
    {
        return name;
    }
    void buy(Shop<T>&, const std::string&);
    //void buy(Shop& s, const char* c) { buy(s,)};
protected:
private:
    T balance;
    std::vector<Item<T>*> items;
    std::string name;
};





template <class T>
Client<T>::Client(T b, std::string n)
{
    balance = b;
    name = n;
    //ctor
#ifdef _DEBUG
    std::cout << "CLIENT ctor" << std::endl;
#endif // _DEBUG
}

template <class T>
Client<T>::~Client()
{
    if (!items.empty())
    {
        for (unsigned int i=0; i<items.size(); i++)
        {
            delete items[i];
        }
    }
    //dtor
#ifdef _DEBUG
    std::cout << "CLIENT dtor" << std::endl;
#endif // _DEBUG
}


template <class T>
void Client<T>::buy(Shop<T>& s, const std::string& n)
{
    Item<T>* i = s-=n;
    if (i==NULL)
        std::cout << "Can't find such item" << std::endl;
    else
    {
        if (balance >= i->get_price())
        {
            balance -= i->get_price();
            items.push_back(i);
#ifdef _DEBUG
            std::cout << "Item bought for " << i->get_price() << ", " << balance << " money left" << std::endl;
#endif // _DEBUG
        }
        else
        {
            s.returnItem(i);
#ifdef _DEBUG
            std::cout << "Item too expensive" << std::endl;
#endif // _DEBUG
        }
    }
}

#endif // CLIENT_H
