#ifndef ITEM_H
#define ITEM_H
#include <string>
#include <iostream>
template <class T>
class Item
{
public:
    Item(T, std::string);

    T get_price() const
    {
        return price;
    }
    std::string get_name() const
    {
        return name;
    }
    int get_id() const
    {
        return id;
    }
    T set_price(T p)
    {
        return price = (p>0)?p:price;
    }

    template <class G>
    friend std::ostream& operator<<(std::ostream& os, const Item<G>& i);

protected:

private:
    T price;
    std::string name;
    int id;
    static int number_of_items;
};








template <class T>
int Item<T>::number_of_items = 0;

template <class T>
Item<T>::Item(T p, std::string n)
{
    price = p;
    name = n;
    id = number_of_items;
    number_of_items++;
    //ctor
#ifdef _DEBUG
    std::cout << "ITEM ctor" << std::endl;
#endif // _DEBUG
}


template <class T>
std::ostream& operator<< (std::ostream& os, const Item<T>& i)
{
    os << "Product name: " << i.name << ", price " << i.price;
    return os;
}




#endif // ITEM_H
