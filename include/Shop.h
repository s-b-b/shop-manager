#ifndef SHOP_H
#define SHOP_H
#include "Item.h"
#include "Employee.h"
#include <vector>
#include <iostream>
template <class T>
class Shop
{
public:
    Shop(std::string = "", std::string = "", T = 1000);
    Shop(const Shop& s);
    Shop(char*, char*, T = 1000);
    virtual ~Shop();

    std::vector<Item<T>*> get_stock() const
    {
        return stock;
    }
    Employee<T> get_emp() const
    {
        return emp;
    }
    static int get_number_of_shop_instances()
    {
        return number_of_shop_instances;
    }
    void operator+=(Item<T>* i)
    {
        stock.push_back(i);
    }
    Item<T>* operator-=(const std::string& n) ;
    void listStock() const; // Why can't be const??? //NVM
    void orderNewItem(const Item<T>);
    void orderNewItem(const std::string&, const T&);
    void returnItem(Item<T>* i)
    {
        stock.push_back(i);
        emp-=i->get_price();
    }

protected:

private:
    std::vector<Item<T>*> stock;
    Employee<T> emp;
    std::string name;
    static int number_of_shop_instances;

};






template <class T>
int Shop<T>::number_of_shop_instances = 0;

template <class T>
Shop<T>::Shop(std::string sname, std::string ename, T sal): emp(ename, sal), name(sname)
{
    number_of_shop_instances++;
    //ctor
#ifdef _DEBUG
    std::cout << "SHOP ctor" << std::endl;
#endif // _DEBUG
}


template <class T>
Shop<T>::Shop(const Shop& s): emp(s.emp), name(s.name)
{
    for (unsigned int i=0; i<s.stock.size(); i++)
    {
        stock.push_back(new  Item<T>(*s.stok[i]));
    }
    number_of_shop_instances++;
    //ctor
#ifdef _DEBUG
    std::cout << "SHOP cctor" << std::endl;
#endif // _DEBUG
}

template <class T>
Shop<T>::Shop(char* sname, char* ename, T sal): emp(std::string(ename), sal), name(sname)
{
    number_of_shop_instances++;
    //ctor
#ifdef _DEBUG
    std::cout << "SHOP ctor" << std::endl;
#endif // _DEBUG
}

template <class T>
Shop<T>::~Shop()
{
    number_of_shop_instances--;
    if (!stock.empty())
    {
        for (unsigned int i=0; i<stock.size(); i++)
        {
            delete stock[i];
        }
    }
    //dtor
#ifdef _DEBUG
    std::cout << "SHOP dtor" << std::endl;
#endif // _DEBUG
}

template <class T>
void Shop<T>::listStock() const
{
    if (!stock.empty())
    {
        for (unsigned int i=0; i<stock.size(); i++)
        {
            std::cout << *stock[i] << std::endl;
        }
    }

}

template <class T>
Item<T>* Shop<T>::operator-=(const std::string& n)
{
    unsigned int i = 0;
    while (i<stock.size())
    {
        if ((*stock[i]).get_name() == n)
            break;
        i++;
    }
    if (i<stock.size())
    {
        Item<T>* it = stock[i];
        stock.erase(stock.begin()+i);
        emp+=it->get_price();
        return it;
    }
    return NULL;
}

template <class T>
void Shop<T>::orderNewItem(const Item<T> i)
{
    if (i.get_price()*8/10 <= emp.get_balance())
    {
        stock.push_back(new Item<T>(i));
        emp-=i.get_price()*8/10;
    }

}

template <class T>
void Shop<T>::orderNewItem(const std::string& c, const T& i)
{
    if (i*8/10 <= emp.get_balance())
    {
        stock.push_back(new Item<T>(i, c));
        emp-=i*8/10;
    }

}







#endif // SHOP_H
