#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include <string>

template <class T>
class Employee
{
public:
    Employee(std::string, T = 1000);

    std::string get_name() const
    {
        return name;
    }
    int get_balance() const
    {
        return balance;
    }
    void operator+=(T i)
    {
        balance += i;
    }
    void operator-=(T i)
    {
        balance -= i;
    }

protected:

private:
    std::string name;
    T balance;
};




template <class T>
Employee<T>::Employee(std::string n, T b)
{
    balance = b;
    name = n;
    //ctor
#ifdef _DEBUG
    std::cout << "SHOP ctor" << std::endl;
#endif // _DEBUG
}



#endif // EMPLOYEE_H
