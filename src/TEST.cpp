#include "Shop.h"
#include "Item.h"
#include "Employee.h"
#include <iostream>
#include <string>
#include <vector>
#include "TEST.h"
#include "Client.h"

static Shop<int> gsi("GSNAME", "GENAME", 4000);
static Item<int> gii(300, "GINAME");

void testShopOperInt()
{
    Shop<int> s((char*)"SNEJM", (char*)"SELLER");
    std::string pre[] = { "Small", "Medium", "Big", "Huge" };

    std::string post[] = { "Teddy", "Doll", "Car", "Motorcycle", "Plane" };
    for (int k=0; k<3; k++)
    {
        for (int i=0; i<4; i++)
        {
            for (int j=0; j<5; j++)
            {
                std::string name = pre[i]+" "+post[j];
                std::cout << name << std::endl;
                Item<int>* it = new Item<int>(100, name);
                s += it;
            }
        }
    }
    s.listStock();
    std::cout << s.get_stock().size() << std::endl;
    Item<int>* m = s-=(char*)"Medium Car";
    std::cout << s.get_stock().size() << std::endl;
    if (m) std::cout << m->get_name() << " " << m->get_price() << std::endl;
    else std::cout << "EMPTY" << std::endl;
}



void typicalScenarioInt()
{
    Shop<int> s((char*)"SNEJM", (char*)"SELLER", 50);
    std::string pre[] = { "Small", "Medium", "Big", "Huge" };
    std::string post[] = { "Teddy", "Doll", "Car", "Motorcycle", "Plane" };
    for (int k=0; k<3; k++)
    {
        for (int i=0; i<4; i++)
        {
            for (int j=0; j<5; j++)
            {
                std::string name = pre[i]+" "+post[j];
                Item<int>* it = new Item<int>(100, name);
                s += it;
            }
        }
    }
    std::cout << "Testing" << std::endl;
    s.orderNewItem("Tiniest Car", 50);
    s.orderNewItem("Tiniest Car", 50);
    std::cout << "Shop emp balance " << s.get_emp().get_balance() << std::endl;
    Client<int> c(270, "name");
    std::string name((char*)"Medium Car");
    c.buy(s, name);
    std::cout << "Shop emp balance " << s.get_emp().get_balance() << std::endl;
    c.buy(s, name);
    std::cout << "Shop emp balance " << s.get_emp().get_balance() << std::endl;
    c.buy(s, name);
    std::cout << "Shop emp balance " << s.get_emp().get_balance() << std::endl;
    if (!c.get_items().empty())
        for (unsigned int i=0; i<c.get_items().size(); i++)
        {
            std::cout << *c.get_items().at(i) << std::endl;
        }
    s.orderNewItem("Tiny Car", 50);
    if (!c.get_items().empty()) s.orderNewItem(*c.get_items().at(0));
    c.buy(s, "Tiny Car");
    c.buy(s, "Tiny Car");
}

template <class T>
void staticTestInnerInt(Shop<T>& s,Item<T>& i)
{
    std::cout << std::endl;
    for(int j=0; j<10; j++)
    {
        s.orderNewItem(i);
        i.set_price((int)(i.get_price()/2));
    }
    std::cout << "Shop stock: " << std::endl;
    s.listStock();
    std::cout << std::endl << std::endl;
    for(int j=0; j<10; j++)
    {
        gsi.orderNewItem(i);
        gii.set_price((int)(gii.get_price()/2));

    }
    std::cout << "Global shop stock: " << std::endl;
    gsi.listStock();
}

void staticTestOuterInt()
{
    static Shop<int> s("SNAME", "ENAME", 2000);
    static Item<int> i(80, "INAME");

    staticTestInnerInt(s, i);
}



template <class T>
void    testShopOperDouble()
{
    Shop<T> s((char*)"SNEJM", (char*)"SELLER");
    std::string pre[] = { "Small", "Medium", "Big", "Huge" };

    std::string post[] = { "Teddy", "Doll", "Car", "Motorcycle", "Plane" };
    for (int k=0; k<3; k++)
    {
        for (int i=0; i<4; i++)
        {
            for (int j=0; j<5; j++)
            {
                std::string name = pre[i]+" "+post[j];
                std::cout << name << std::endl;
                Item<T>* it = new Item<T>(100.93, name);
                s += it;
            }
        }
    }
    s.listStock();
    std::cout << s.get_stock().size() << std::endl;
    Item<T>* m = s-=(char*)"Medium Car";
    std::cout << s.get_stock().size() << std::endl;
    if (m) std::cout << m->get_name() << " " << m->get_price() << std::endl;
    else std::cout << "EMPTY" << std::endl;
}


template <class T>
void typicalScenarioDouble()
{
    Shop<T> s((char*)"SNEJM", (char*)"SELLER", 5000.76);
    std::string pre[] = { "Small", "Medium", "Big", "Huge" };
    std::string post[] = { "Teddy", "Doll", "Car", "Motorcycle", "Plane" };
    for (int k=0; k<3; k++)
    {
        for (int i=0; i<4; i++)
        {
            for (int j=0; j<5; j++)
            {
                std::string name = pre[i]+" "+post[j];
                Item<T>* it = new Item<T>(100.8, name);
                s += it;
            }
        }
    }
    std::cout << "Testing" << std::endl;
    s.orderNewItem("Tiniest Car", 50.4);
    s.orderNewItem("Tiniest Car", 50.4);
    std::cout << "Shop emp balance " << s.get_emp().get_balance() << std::endl;
    Client<T> c(270.54, "name");
    std::string name((char*)"Medium Car");
    c.buy(s, name);
    std::cout << "Shop emp balance " << s.get_emp().get_balance() << std::endl;
    c.buy(s, name);
    std::cout << "Shop emp balance " << s.get_emp().get_balance() << std::endl;
    c.buy(s, name);
    std::cout << "Shop emp balance " << s.get_emp().get_balance() << std::endl;
    if (!c.get_items().empty())
        for (unsigned int i=0; i<c.get_items().size(); i++)
        {
            std::cout << *c.get_items().at(i) << std::endl;
        }
    s.orderNewItem("Tiny Car", 50.4);
    if (!c.get_items().empty()) s.orderNewItem(*c.get_items().at(0));
    c.buy(s, "Tiny Car");
    c.buy(s, "Tiny Car");
}
















void testall()
{
    std::cout << "\t\t######TESTING INT######" << std::endl;
    staticTestOuterInt();
    testShopOperInt();
    typicalScenarioInt();
    staticTestOuterInt();
    std::cout << "\t\t######TESTING DOUBLE######" << std::endl;
    testShopOperDouble<double>();
    typicalScenarioDouble<double>();
    std::cout << "\t\t######TESTING FLOAT######" << std::endl;
    testShopOperDouble<float>();
    typicalScenarioDouble<float>();
}
